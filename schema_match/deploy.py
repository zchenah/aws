#!/usr/bin/env python
# coding:utf-8
__author__ = 'Chen Zhao'
import sys
from fabric.api import env
from fabric.context_managers import cd
from fabric.operations import put, sudo, run, local
from fabric.contrib.project import rsync_project
import os.path



env.host_string = "swarm@csz908.cse.ust.hk"
env.key_filename = '/Users/chin/.ssh/chin_rsa'
env.password = 'swarm'

exclude_list = ['.git*', '*.pyc', '.idea', '*.sqlite',]

def local():
    rsync_project(remote_dir='/home/swarm/aws/', exclude=exclude_list, delete=False)

def remote():
    sudo('apache2ctl graceful')
    with cd('/home/swarm/aws/schema_match'):
        # run('rm ./db/*')
        run('python manage.py syncdb')
        # run('python manage.py createsuperuser')
        run('python manage.py migrate')
        sudo('chmod 777 db')
        sudo('chmod 777 db/*')
        run('python manage.py collectstatic --noinput')

def main():
    local()
    remote()
    pass

if __name__=='__main__':
    main()


