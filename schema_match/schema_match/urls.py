from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()



mturk_urls = patterns('mturk.views',
                      url(r'^hits', 'create'),
                      url(r'^answers', 'on_submit_answer'),
                      url(r'^external-hits/(?P<id>\d+)', 'get_external_hit_for_iframe'),
                      )

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'schema_match.views.home', name='home'),
    # url(r'^schema_match/', include('schema_match.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^mturk/', include(mturk_urls)),

)
