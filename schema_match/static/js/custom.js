/**
 * Created with PyCharm.
 * User: chin
 * Date: 15/8/13
 * Time: 3:41 PM
 * To change this template use File | Settings | File Templates.
 */



// using jQuery
// useless until we enable csrf
(function(){
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            console.log('set csrf');
            xhr.setRequestHeader("X-CSRF-Token", csrftoken);
        }
    });
})();
