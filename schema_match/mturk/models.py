# coding:utf8

from django.db import models
import adaptor

# Create your models here.



class Worker(models.Model):
    mturk_worker_id = models.CharField(max_length=50, unique=True)
    created_on = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return "[Worker: ...%s]"%self.mturk_worker_id[:4]


class ExHIT(models.Model):
    mturk_hit_id = models.CharField(max_length=50, unique=True)
    url = models.URLField()
    content = models.TextField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.mturk_hit_id:
            self.mturk_hit_id = adaptor.create_hit()
        super(ExHIT, self).save(*args, **kwargs) # Call the "real" save() method.

    def __unicode__(self):
        return "[HIT: ...%s | %s]"%(self.mturk_hit_id[:4], self.url)

class Assignment(models.Model):
    hit = models.ForeignKey('ExHIT')
    worker = models.ForeignKey('Worker')
    mturk_assignment_id = models.CharField(max_length=50, unique=True)
    content = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return "[Assignment: ...%s | %s | %s | %s]"%(self.mturk_assignment_id[:4], self.hit, self.worker, self.content)

class Answer(models.Model):
    assignment = models.ForeignKey('Assignment')
    content = models.BooleanField()
    created_on = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return "[Answer: %s | %s]"%(self.assignment,  self.content)


