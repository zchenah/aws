__author__ = 'chin'

from django.contrib import admin
from models import *


class ExHITAdmin(admin.ModelAdmin):
    fields = ('content',)



admin.site.register(Worker)
admin.site.register(ExHIT, ExHITAdmin)
admin.site.register(Assignment)
admin.site.register(Answer)
