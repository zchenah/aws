# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response, render
import datetime
from models import *

def create(request):
    pass

def on_submit_answer(request):
    assignment = Assignment.objects.get(mturk_assignment_id=request.REQUEST['assignmentId'])
    bool_res = request.REQUEST['content'] == 'true'
    Answer.objects.create(assignment=assignment, content=bool_res)
    return HttpResponse(content="OK")


def next_question(worker, hit):
    assignments = Assignment.objects.filter(worker=worker)
    return {'text':
                'This is the question content of HIT: %s for worker: %s'%(hit.mturk_hit_id, worker.mturk_worker_id),
            'debug':'The worker has answered %d questions'%(len(assignments))
    }


def get_external_hit_for_iframe(request, id):
    assignmentId = request.REQUEST['assignmentId']
    if assignmentId=='ASSIGNMENT_ID_NOT_AVAILABLE':
        return render(request, 'external_hit_for_preview.html')
    worker, created =  Worker.objects.get_or_create(mturk_worker_id=request.REQUEST['workerId'])
    hit, created =  ExHIT.objects.get_or_create(mturk_hit_id=request.REQUEST['hitId'])
    question = next_question(worker, hit)
    assignment, created = Assignment.objects.get_or_create(mturk_assignment_id=assignmentId,
                                          worker=worker,
                                          hit=hit,
                                          content=question['text'])
    #mturkSubmitURL = 'https://workersandbox.mturk.com/mturk/externalSubmit'  #request.REQUEST['turkSubmitTo']
    return render(request, 'external_hit.html', {'question':question })


